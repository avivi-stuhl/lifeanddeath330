#ifndef BUTTON_HPP_
#define BUTTON_HPP_

#include <SDL2/SDL.h>

class Button{
    public:
        Button(int x, int y, int h, int w);
        bool isClicked(int mouse_x, int mouse_y);
        SDL_Rect getBox();
    private:
        SDL_Rect box;
};

#endif