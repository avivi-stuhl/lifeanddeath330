#ifndef BOARD_HPP_
#define BOARD_HPP_

#include "tile.hpp"
#include <memory>
#include "pattern.hpp"
using namespace std;

class Board {
    public:
        Board(int h, int w);
        ~Board(){
            for(int i = 0; i < height; i++){
                delete [] boardPtr[i];
            }
            delete [] boardPtr;
        }
        void init();
        void traverse();
        Tile** getBoardPtr();
        void checkNeighbors(int x, int y);
	    // void changeTiles();
        void setTilesNextColor();
        void setTilesColor();
        int getHeight();
        int getWidth();
        Color neighborsColor(int x, int y);
        Tile **boardPtr;
        void makeMovePreview(int x, int y, int player);
        int countColor(Color c);
        void undo(int x, int y);
        void setFromPattern(Pattern p);
        
    private:
        int height = 21;
        int width = 21;     
};

#endif  /* BOARD_HPP_ */
