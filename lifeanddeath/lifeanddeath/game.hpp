#ifndef GAME_HPP_
#define GAME_HPP_

#include <SDL2/SDL.h>
#include "board.hpp"
#include "button.hpp"

class Game{
    public:
        Game();
        void init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen, Board* myBoard, Button* myButton1, Button* myButton2, Button *myButton3, Button *myButton);

        bool handleEvent(int x, int y);
        bool update();
        void render();
        void clean();
        bool inBoard(int x1, int y1, int x2, int y2);
        void renderPlayerInfo();
        void renderButtons(SDL_Rect b1, SDL_Rect b2, SDL_Rect b3, SDL_Rect b4);
        void renderPlayerTotal();

        // returns a bool indicating whether the game is still running
        bool running(){return isRunning; };
        
    private:
        SDL_Window *window = NULL;
        SDL_Renderer *renderer = NULL;
        Board *board;
        Button *confirmButton;
        Button *undoButton;
        Button *patternButton;
        Button *patternButton2;
        bool isRunning;
        int player = 1;
        int moveX;  // last move made
        int moveY;
        int redTotal;
        int blueTotal;
	    bool madeMove = false;
        
};

#endif /* GAME_HPP_ */
