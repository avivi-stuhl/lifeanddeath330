#include <iostream>
#include <SDL2/SDL.h>
using namespace std;

#include "game.hpp"
#include "button.hpp"

const int SCREEN_WIDTH = 1200, SCREEN_HEIGHT=1200;

int main(int argc, char *argv[]){

    SDL_Event event;

    Board board(19, 19);
    board.init();
    Button button1(630, 350, 90, 40);
    Button button2(630, 400, 90, 40);
    Button button4(630, 300, 90, 40);
    Button button3(630, 250, 90, 40);
    // board.printBoard();
    Game *game;
    game = new Game();
    
    game->init("Life and Death", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, false, &board, &button1, &button2, &button3, &button4);

    bool quit = false;
    int x,y;

    while (!quit){
        while(SDL_PollEvent(&event)){
            switch(event.type){
                case SDL_QUIT:
                    quit = true;
                    break;
            
                // changes by pressing any key
                case SDL_KEYDOWN:
                    game->update();
                    game->render();

                case SDL_MOUSEBUTTONDOWN:
                    SDL_GetMouseState(&x, &y);   // get the exact coordinates of where the mouse is clicked
                    quit = game->handleEvent(x, y); 
                    game->render();  
                    break;
            }
        }   
    }

    game->clean();

    return 0;
}
