#include <iostream>
#include <SDL2/SDL.h>
using namespace std;
#include "button.hpp"

Button::Button( int x, int y, int w, int h ){
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;

}

bool Button::isClicked(int mouse_x, int mouse_y){
    return (mouse_x >= box.x) && (mouse_y >= box.y) &&
           (mouse_x < box.x + box.w) && (mouse_y < box.y + box.h);
}

SDL_Rect Button::getBox(){
    return box;
}

