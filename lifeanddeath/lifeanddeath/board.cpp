#include <iostream>
#include <random>
#include <algorithm>
#include <stdlib.h>
#include <memory>
using namespace std;
#include "board.hpp"

// constructor
Board::Board(int h, int w){
    height = h;
    width = w;
    boardPtr = new Tile*[height];
    for(int i = 0; i < height; i++){
        boardPtr[i] = (Tile *)new Tile[width];
    }
}

int Board::getHeight(){
    return height;
}

int Board::getWidth(){
    return width;
}

// create randomly generated board with 2 mirrored sides
void Board::init(){
    int rand_color;
    random_device rd;

    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            if(i == j){
                rand_color = 0;
            }
            else if(i != j){
                //rand_color = rand() % 3;   // generate random number in range 0-2
                mt19937 gen(rd());
                uniform_int_distribution<int> dis(0, 2);
                rand_color = dis(gen);
            }
            Tile tile;
            tile.init(i, j, height, width);
            tile.changeCol(rand_color);
            boardPtr[i][j] = tile;
            // mirror the opposite color on the other half of board
            Tile tile2;
            tile2.init(j, i, height, width);
            if(rand_color == 2){
                rand_color = 1;
            }
            else if(rand_color == 1){
                rand_color = 2;
            }
            tile2.changeCol(rand_color);
            boardPtr[j][i] = tile2;  
        }
    }
}

Tile** Board::getBoardPtr(){
    return boardPtr;
}

// travese the whole board and change the tiles next color for preview
void Board::traverse(){
	for(int i = 0; i < height; i++){
		for(int j = 0; j < width; j++){
			checkNeighbors(i, j);
		}
	}
	setTilesNextColor();
}

void Board::checkNeighbors(int x, int y){
    Tile curNeigh;
    int coutLive = 0;
    // traverse the neighbors of the tile
    // count the ones that are alive and store it into the field for the tile
    for (int i=0; i<boardPtr[x][y].num_neighbors; i++){
        curNeigh = boardPtr[boardPtr[x][y].neighbors[i][0]][boardPtr[x][y].neighbors[i][1]];
        if (curNeigh.getColor() != 0){
            coutLive += 1;
        }
    }
    boardPtr[x][y].living_n = coutLive;
}

Color Board::neighborsColor(int x, int y){
    Tile curNeigh;
    int coutR = 0;
    int coutB = 0;
    // traverse the neighbors of the tile
    // count the ones that are alive and store it into the field for the tile
    for (int i=0; i<boardPtr[x][y].num_neighbors; i++){
        curNeigh = boardPtr[boardPtr[x][y].neighbors[i][0]][boardPtr[x][y].neighbors[i][1]];
        if (curNeigh.getColor() == r){
            coutR += 1;
        }else if(curNeigh.getColor() == b){
            coutB += 1;
	    }
    }
    if (coutR > coutB){
    	return r;
    }else{
    	return b;
    }
		
}

// set the nextCol for a board for preview
void Board::setTilesNextColor(){
	for(int i = 0; i < height; i++){
		for(int j = 0; j < width; j++){
            if(boardPtr[i][j].getColor() == e){	// if it's empty
				if(boardPtr[i][j].living_n == 3){
					Color bornCol = neighborsColor(i, j);
					boardPtr[i][j].willBorn(bornCol);	// will have to be fixed to right color	
				}else{
					boardPtr[i][j].willDie();
				}

			}else{	// if it's not empty
				if(boardPtr[i][j].living_n < 2){	// lonliness
					boardPtr[i][j].willDie();
				}else if(boardPtr[i][j].living_n > 3){	// overpopulation
					boardPtr[i][j].willDie();
				}else{
					Color tileCol = boardPtr[i][j].getColor();
					boardPtr[i][j].willBorn(tileCol);
				}
			}
		}
	}
}

// set the col of the tiles to be nextCol
void Board::setTilesColor(){
    for(int i = 0; i < height; i++){
		for(int j = 0; j < width; j++){
            boardPtr[i][j].setColor();
        }
    }
    traverse();
}

void Board::setFromPattern(Pattern p){
    
	for(int x = 0; x < height; x++){
		for(int y = 0; y < width; y++){
			boardPtr[x][y].changeCol(0);
		}
	}
	int cur_col = 1;
	for(int i = 0; i < p.tuple_num; i++){
		boardPtr[p.tuples[i][0]][p.tuples[i][1]].changeCol(cur_col);
		if(cur_col == 1){
			cur_col = 2;
		}else{
			cur_col = 1;
		}
	}
}

// change the next color field for the tile
void Board::makeMovePreview(int x, int y, int player){
    if (boardPtr[x][y].getColor() == e){
		boardPtr[x][y].setNextColor(player);
	}else {	// if it has a color
		boardPtr[x][y].setNextColor(e);
	}
}

int Board::countColor(Color c){
	int count = 0;
	for(int i = 0; i < height; i++){
		for(int j = 0; j < width; j++){
			if(boardPtr[i][j].getColor() == c){
				count++;
			}
		}
	}
	return count;
}

void Board::undo(int x, int y){
	boardPtr[x][y].undoColor();	
}


