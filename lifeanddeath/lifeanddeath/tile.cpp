#include "tile.hpp"
#include <iostream>
#include <SDL2/SDL.h>
using namespace std;

Tile::Tile(){
	col = e;
}

void Tile::init(int x, int y, int h, int w){
	setRect(100+x*25, 100+y*25, 24, 24);
	setSmallerRect(100+x*25+8, 100+y*25+8, 8, 8);
	if(x == 0 && y == 0){	// corner 0,0
		num_neighbors = 3;
		neighbors[0][0] = x + 1;
		neighbors[0][1] = y;
		neighbors[1][0] = x + 1;
		neighbors[1][1] = y + 1;
		neighbors[2][0] = x;
		neighbors[2][1] = y + 1;
	}else if(x == 0 && y == w - 1){
		num_neighbors = 3;
		neighbors[0][0] = x + 1;
		neighbors[0][1] = y;
		neighbors[1][0] = x + 1;
		neighbors[1][1] = y - 1;
		neighbors[2][0] = x;
		neighbors[2][1] = y - 1;
	}else if(x == h - 1 && y == 0){
		num_neighbors = 3;
		neighbors[0][0] = x - 1;
		neighbors[0][1] = y;
		neighbors[1][0] = x - 1;
		neighbors[1][1] = y + 1;
		neighbors[2][0] = x;
		neighbors[2][1] = y + 1;
	}else if(x == h - 1 && y == w - 1){
		num_neighbors = 3;
		neighbors[0][0] = x - 1;
		neighbors[0][1] = y;
		neighbors[1][0] = x - 1;
		neighbors[1][1] = y - 1;
		neighbors[2][0] = x;
		neighbors[2][1] = y - 1;
	
	}else if(x == 0){	// side
		num_neighbors = 5;
		neighbors[0][0] = x;
		neighbors[0][1] = y + 1;
		neighbors[1][0] = x;
		neighbors[1][1] = y - 1;
		neighbors[2][0] = x + 1;
		neighbors[2][1] = y - 1;
		neighbors[3][0] = x + 1;
		neighbors[3][1] = y;
		neighbors[4][0] = x + 1;
		neighbors[4][1] = y + 1;
	}else if(y == 0){
		num_neighbors = 5;
		neighbors[0][0] = x - 1;
		neighbors[0][1] = y;
		neighbors[1][0] = x + 1;
		neighbors[1][1] = y;
		neighbors[2][0] = x - 1;
		neighbors[2][1] = y + 1;
		neighbors[3][0] = x;
		neighbors[3][1] = y + 1;
		neighbors[4][0] = x + 1;
		neighbors[4][1] = y + 1;
	}else if(x == h - 1){
		num_neighbors = 5;
		neighbors[0][0] = x;
		neighbors[0][1] = y + 1;
		neighbors[1][0] = x;
		neighbors[1][1] = y - 1;
		neighbors[2][0] = x - 1;
		neighbors[2][1] = y - 1;
		neighbors[3][0] = x - 1;
		neighbors[3][1] = y;
		neighbors[4][0] = x - 1;
		neighbors[4][1] = y + 1;
	}else if(y == w - 1){
		num_neighbors = 5;
		neighbors[0][0] = x - 1;
		neighbors[0][1] = y;
		neighbors[1][0] = x + 1;
		neighbors[1][1] = y;
		neighbors[2][0] = x - 1;
		neighbors[2][1] = y - 1;
		neighbors[3][0] = x;
		neighbors[3][1] = y - 1;
		neighbors[4][0] = x + 1;
		neighbors[4][1] = y - 1;
	
	}else{	// "middle", as in not side or corner
		num_neighbors = 8;
		neighbors[0][0] = x - 1;
		neighbors[0][1] = y - 1;
		neighbors[1][0] = x - 1;
		neighbors[1][1] = y;
		neighbors[2][0] = x - 1;
		neighbors[2][1] = y + 1;
		neighbors[3][0] = x;
		neighbors[3][1] = y - 1;
		neighbors[4][0] = x;
		neighbors[4][1] = y + 1;
		neighbors[5][0] = x + 1;
		neighbors[5][1] = y - 1;
		neighbors[6][0] = x + 1;
		neighbors[6][1] = y;
		neighbors[7][0] = x + 1;
		neighbors[7][1] = y + 1;
	}
	
}

Tile::~Tile(){
}

void Tile::setRect(int xpos, int ypos, int h, int w){
	square.x = xpos;
    square.y = ypos;
    square.w = w;
    square.h = h;
}

void Tile::setSmallerRect(int xpos, int ypos, int h, int w){
	smaller.x = xpos;
    smaller.y = ypos;
    smaller.w = w;
    smaller.h = h;
}

SDL_Rect Tile::getRect(){
	return square;
}

SDL_Rect Tile::getSmaller(){
	return smaller;
}

// check if the mouse clicked area is in a tile's square
bool Tile::inRect(int x, int y){
	return (x >= square.x) && (y >= square.y) &&
           (x < square.x + square.w) && (y < square.y + square.h);
}

void Tile::willDie(){
	
	nextCol = e;
	prevNextCol = nextCol;
}

void Tile::willBorn(Color c){
	nextCol = c;
	prevNextCol = nextCol;
}

Color Tile::getColor(){
	return col;
}

Color Tile::getNextColor(){
	return nextCol;
}

void Tile::setColor(){
	col = nextCol;
}

void Tile::changeCol(int num){
	setNextColor(num);
	setColor();
}

void Tile::setNextColor(int num){
	if(num == 2){
		nextCol = b;
	}
	else if(num == 1){
		nextCol = r;
	}
	else{
		nextCol = e;
	}
}

void Tile::undoColor(){
	nextCol = prevNextCol;
}

