#ifndef TILE_HPP_
#define TILE_HPP_

#include <SDL2/SDL.h>

typedef enum{
	e, r, b 	// empty, red, blue
}Color;

class Tile{
	public:
		Tile();
		~Tile();
		void init(int x, int y, int h, int w);
		void willDie();
		void willBorn(Color c);
		Color getColor();
		Color getNextColor();
		void setColor();
		void setNextColor(int num);
		void changeCol(int num);
		void setRect(int xpos, int ypos, int h, int w);
		void setSmallerRect(int xpos, int ypos, int h, int w);
		bool inRect(int x, int y);
		SDL_Rect getRect();
		SDL_Rect getSmaller();
		int neighbors[8][2];	// array of tuples with coordinates
		int num_neighbors;
		int living_n = -1;
		void undoColor();
	private:
		Color col;
		SDL_Rect square;
		Color nextCol;
		SDL_Rect smaller;
		Color prevNextCol;
};
#endif 
