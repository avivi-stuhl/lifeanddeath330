#include <iostream>
#include <string>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
using namespace std;
#include "game.hpp"
#include "button.hpp"
#include "tile.hpp"
#include "pattern.hpp"

Game::Game(){
    
}

void Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen, Board* myBoard, Button* myButton1, Button* myButton2, Button* myButton3, Button *myButton4){
   
    // check if SDL2 is initialized
   if (SDL_Init(SDL_INIT_EVERYTHING) == 0){
       cout << "Subsystem Initialized" << endl;

       if(!TTF_WasInit() && TTF_Init()==-1) {
            cout << "TTF_Init: \n" << endl;
            throw TTF_GetError();
            exit(1);
        }

        // create the window
        
        window = SDL_CreateWindow("Life&Death", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
        if( window == NULL){
            cout << "Window could not be created! SDL_Error: " << endl;
            throw SDL_GetError();
        }
        // create renderer
        renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED);

        if (renderer){
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            cout << "Renderer created !" << endl;
        }
        board = myBoard;
        confirmButton = myButton1;
        undoButton = myButton2;
        patternButton = myButton3;
        patternButton2 = myButton4;
        isRunning = true;

    }else {
       isRunning = false;
    }

}

bool Game::handleEvent(int x, int y){
    Tile** board_pointer= board->getBoardPtr();
    int board_top_left_x = board_pointer[0][0].getRect().x;
    int board_top_left_y = board_pointer[0][0].getRect().y;
    bool quit = false;
    
    if(inBoard(x, y, board_top_left_x, board_top_left_y)  && !madeMove){
        // if mouse clicked inside board, find which tile was clicked
        // find the tile index
       
        int tileX = (x - 100) / 25;
        int tileY = (y - 100) / 25;

        moveX = tileX;
        moveY = tileY;
        Tile tile = board_pointer[tileX][tileY];
        
        board->makeMovePreview(tileX, tileY, player);             
         // this set the next-col field for all the tiles
	    madeMove = true;

        // board->printBoard();
    }

    // if confirm button is clicked
    if(confirmButton -> isClicked(x, y)){
        quit = update(); // this updates the tiles to be the new color
        // board->traverse();
        madeMove = false;
        // update the player each time
        if (player == 1){
            player = 2;
        } else {
            player = 1;
        } 
    }

    // if undo button is clicked
    if(undoButton -> isClicked(x, y)){
        
        // update the player each time
        board->undo(moveX, moveY);
        madeMove = false;
         
    } 

    if (patternButton -> isClicked(x,y)){
        int myList[50] = {
		9, 4, 
		9, 5,
		9, 6,
		9, 7,
		9, 8,
		9, 9,
		9, 10,
		9, 11,
		9, 12,
		9, 13,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        Pattern cell10(10, myList);
	    board->setFromPattern(cell10);
    }

    if(patternButton2-> isClicked(x, y)){
    	int myList[50] = {
		7, 7,
		7, 8,
		7, 10,
		7, 11,
		8, 7,
		8, 8,
		8, 10,
		8, 11,
		9, 8,
		9, 10,
		10, 6,
		10, 8,
		10, 10,
		10, 12,
		11, 6,
		11, 8,
		11, 10,
		11, 12,
		12, 6,
		12, 7,
		12, 11,
		12, 12,
		0, 0, 0, 0, 0, 0};
	Pattern tumblr(22, myList);
	board->setFromPattern(tumblr);
    }

    return quit;
}

// check if mouse clicked inside board
bool Game::inBoard(int x1, int y1, int x2, int y2){
    int h = board -> getHeight();
    int w = board -> getWidth();
    return (x1 >= x2) && (y1 >= y2) &&
           (x1 < x2 + w*25) && (y1 < y2 + h*25);
}

// change the colors of tile to be next color
bool Game::update(){
    board->setTilesColor();
    redTotal = board->countColor(r);
    blueTotal = board->countColor(b);

    if(redTotal == 0){
    	cout << "Blue Wins!\n";
	    // clean();
        return true;
    }else if(blueTotal == 0){
    	cout << "Red Wins!\n";
	    // clean();
        return true;
    } else {
        return false;
    }
}

void Game::render(){
    Tile** board_pointer= board -> getBoardPtr();
    // SDL_RenderClear(renderer);

    int h = board -> getHeight();
    int w = board -> getWidth();

    SDL_Rect background;
    background.x = 95;
    background.y = 95;
    background.w = 19*25+10;
    background.h = 19*25+10;
    SDL_SetRenderDrawColor(renderer, 100, 100, 100, 0);
    SDL_RenderFillRect(renderer, &background);

    for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++){
            Tile tile = board_pointer[i][j];
            SDL_Rect rect = tile.getRect();
            SDL_Rect smaller = tile.getSmaller();

            if(tile.getColor() == e){
                // tile is empty, set color to black
                SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            }
            else if(tile.getColor() == r){
                // tile is red
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
            }
            else{
                // tile is blue
                SDL_SetRenderDrawColor(renderer, 0, 0, 255, 0);
            }
            SDL_RenderFillRect(renderer, &rect);
            
            if(tile.getNextColor() == e){
                // tile will be empty, set color to black
                SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            }
            else if(tile.getNextColor() == r){
                // tile will be red
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
            }
            else{
                // tile will be blue
                SDL_SetRenderDrawColor(renderer, 0, 0, 255, 0);
            }
            SDL_RenderFillRect(renderer, &smaller);
        }
    }
    // draw button 
    SDL_Rect confirm_rect = confirmButton -> getBox();
    SDL_Rect undo_rect = undoButton -> getBox();
    SDL_Rect pattern_rect = patternButton -> getBox();
    SDL_Rect pattern_rect2 = patternButton2 -> getBox();
    renderButtons(confirm_rect, undo_rect, pattern_rect, pattern_rect2);
    // renders current player indicator
    renderPlayerInfo();
    renderPlayerTotal();

    SDL_RenderPresent(renderer);

    // Wait for 0.5 sec
    SDL_Delay( 500 );
}


// create confirm and cancel buttons
void Game::renderButtons(SDL_Rect b1, SDL_Rect b2, SDL_Rect b3, SDL_Rect b4){
    
    SDL_SetRenderDrawColor(renderer, 34, 139, 34, 0);
    TTF_Font* fontB = TTF_OpenFont("data-latin.ttf", 18);
    SDL_Color textColorB = { 255, 255, 255, 255 }; // white
    SDL_RenderFillRect(renderer, &b1);

    SDL_Surface* surf = TTF_RenderText_Solid( fontB, "CONFIRM", textColorB );
    SDL_Texture* text;
	text = SDL_CreateTextureFromSurface( renderer, surf );
    SDL_RenderCopy( renderer, text, nullptr, &b1 );

    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
    SDL_RenderFillRect(renderer, &b2);

    SDL_Surface* surf2 = TTF_RenderText_Solid( fontB, "UNDO", textColorB );
    SDL_Texture* text2;
	text2 = SDL_CreateTextureFromSurface( renderer, surf2 );
    SDL_RenderCopy( renderer, text2, nullptr, &b2 );

    SDL_SetRenderDrawColor(renderer, 100, 0, 60, 0);
    SDL_RenderFillRect(renderer, &b3);

    SDL_Surface* surf3 = TTF_RenderText_Solid( fontB, "pattern1", textColorB );
    SDL_Texture* text3;
	text3 = SDL_CreateTextureFromSurface( renderer, surf3 );
    SDL_RenderCopy( renderer, text3, nullptr, &b3 );

    SDL_SetRenderDrawColor(renderer, 100, 0, 60, 0);
    SDL_RenderFillRect(renderer, &b4);

    SDL_Surface* surf4 = TTF_RenderText_Solid( fontB, "pattern2", textColorB );
    SDL_Texture* text4;
	text4 = SDL_CreateTextureFromSurface( renderer, surf4 );
    SDL_RenderCopy( renderer, text4, nullptr, &b4 );

    SDL_FreeSurface( surf );
    SDL_FreeSurface( surf2 );
    SDL_FreeSurface( surf3 );
    SDL_FreeSurface( surf4 );
}

void Game::renderPlayerTotal(){
    SDL_Rect totalRRect;
    SDL_Rect totalBRect;
    SDL_Rect RRect;
    SDL_Rect BRect;

    TTF_Font* font = TTF_OpenFont("data-latin.ttf", 24);
    SDL_Color textColor = { 255, 255, 255, 255 }; // white
    SDL_Color bgColor = {0, 0, 0, 0};

    SDL_Surface* surf1 = TTF_RenderText_Shaded( font, "Red: ", textColor, bgColor);
    SDL_Texture* text1;
	text1 = SDL_CreateTextureFromSurface( renderer, surf1 );

    SDL_Surface* surf2 = TTF_RenderText_Shaded( font, "Blue: ", textColor, bgColor);
    SDL_Texture* text2;
	text2 = SDL_CreateTextureFromSurface( renderer, surf2 );

    char strRed[20];
    sprintf (strRed, "%d", redTotal);
    SDL_Surface* surf3 = TTF_RenderText_Shaded( font, strRed, textColor, bgColor);
    SDL_Texture* text3;
	text3 = SDL_CreateTextureFromSurface( renderer, surf3 );

    char strBlue[20];
    sprintf (strBlue, "%d", blueTotal);
    SDL_Surface* surf4 = TTF_RenderText_Shaded( font, strBlue, textColor, bgColor);
    SDL_Texture* text4;
	text4 = SDL_CreateTextureFromSurface( renderer, surf4 );

    totalRRect.x = 680;
    totalRRect.y = 150;
    totalRRect.w = 30;
    totalRRect.h = 20;

    totalBRect.x = 680;
    totalBRect.y = 170;
    totalBRect.w = 30;
    totalBRect.h = 20;

    RRect.x = 620;
    RRect.y = 150;
    RRect.w = 60;
    RRect.h = 20;

    BRect.x = 620;
    BRect.y = 170;
    BRect.w = 60;
    BRect.h = 20;

    SDL_RenderCopy( renderer, text3, nullptr, &totalRRect ); 
    SDL_RenderCopy( renderer, text4, nullptr, &totalBRect );

    SDL_RenderCopy( renderer, text1, nullptr, &RRect ); 
    SDL_RenderCopy( renderer, text2, nullptr, &BRect );

    SDL_FreeSurface( surf1 );
    SDL_FreeSurface( surf2 );
    SDL_FreeSurface( surf3 );
    SDL_FreeSurface( surf4 );
}
// create a current-player indicator
void Game::renderPlayerInfo(){
    
    // solidRect to hold text
    SDL_Rect solidRect;
    // playerRect to hold the color of current player
    SDL_Rect playerRect;

    SDL_Rect solidRect2;

    TTF_Font* font = TTF_OpenFont("data-latin.ttf", 24);
    SDL_Color textColor = { 255, 255, 255, 255 }; // white

    // create the surface
    SDL_Surface* surf1 = TTF_RenderText_Solid( font, "current:", textColor );
    SDL_Texture* text1;
	text1 = SDL_CreateTextureFromSurface( renderer, surf1 );

    SDL_Surface* surf2 = TTF_RenderText_Solid( font, "Game Of Life and Death", textColor );
    SDL_Texture* text2;
	text2 = SDL_CreateTextureFromSurface( renderer, surf2 );

    // set coordinates for rects
    
    // 'current' rect
	solidRect.x = 600;
	solidRect.y = 100;
    solidRect.w = 100;
    solidRect.h = 30;

    // player color rect
    playerRect.x = 700;
    playerRect.y = 110;
    playerRect.w = 20;
    playerRect.h = 10;

    // title rect
    solidRect2.x = 90;
	solidRect2.y = 10;
    solidRect2.w = 500;
    solidRect2.h = 60;
    
    
    SDL_RenderCopy( renderer, text1, nullptr, &solidRect ); 
    SDL_RenderCopy( renderer, text2, nullptr, &solidRect2 ); 
    

    // set color of current player
    if (player == 1){
        // current player is red
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
    } else{
        // current player is blue
        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 0);
    }

    SDL_RenderFillRect(renderer, &playerRect);
    
    SDL_FreeSurface( surf1 );
    SDL_FreeSurface( surf2 );
    
}

// free game
void Game::clean(){
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    cout << "Game Cleaned" << endl;
}
