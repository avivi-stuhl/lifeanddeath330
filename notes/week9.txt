summary:
    what we did:
        1. Embed SDL2(garphics) to the Game
           built functions to handle mouse click inputs
           added square field to each tile to distinguish which tile user wanted to change
        2. added SDL2_ttf library
            added this library to render text onto the SDL window
            built a current-player indicator based on it
        3. user-input (mouse click event) / event-listener and event-handler
            user can now click on board and change tiles
        4. generate random symmetric board to add fairness to the game
        5. added preview, undo functionalities to allow users to see what the board will look like with their intended move
        6. added confirm and undo buttons



    obstacles met:
        1. the x/y order is different for SDL2 window than what we had in the command line, we need to change it
        2. the color of game background changes when it's not supposed to (SDL2 renderer issues)
        3. found out that our current logic might only work with automatic game sessions,
           need to figure out how to develop user-input ones
        4. SDL2 event issues: sometimes SDL2 will change the color of the tile when we only place it on top
            (we only want it to change when we click it)
        5. SDL2 issuse:
            we didn't know that moving a cursor is also an event..
        6. random issue:
            haing trouble of coming up with how to make a ramdom board
            (some methods are too slow, some are not fair)
        7. SDL2_tff won't work (header files path issues)
            had to re-download the library
        8. Preview and Undo functionalities
        
        
    team coordination activities:
        1. meeting on Tuesday to work on event-handler for SDL2 (Jeanie, Zoey)
        2. meeting on Thursday to work on further logic checking (Nitan, Zoey)
        3. meeting on Friday to add random board generator and game without user input (Jeanie, Zoey)
        4. meeting on Sunday to work on more features for the game (everyone)

    plan for next week:
        1. Create powerpoint
        2. Do last checking
        
        