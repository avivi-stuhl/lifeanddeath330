** Project name:   Game of life and death

** Project repository: https://avivi-stuhl@bitbucket.org/avivi-stuhl/lifeanddeath330.git

** Team members: Nitan Avivi, Jeanie Chen, Zoey Zhao

** Description (1-3 paragraphs): 
Conway’s game of life is a simple zero player game devised by John Conway which means to simulate cellular life.  
The rules are:
1. Any live cell with fewer than two live neighbors dies, as if by underpopulation.
	2.Any live cell with two or three live neighbors lives on to the next generation.
	3.Any live cell with more than three live neighbors dies, as if by overpopulation.
	4.Any dead cell with exactly three live neighbors becomes a live cell, as if by  
reproduction.
Game of life and death was created by Youtuber carykh which turns Conway’s game of life into a two player competitive game.  Each cell belongs to one of the players.  When a new cell is born, it belongs to the player who controls more neighboring cells.  Players take turns killing cells or creating new ones.
 

** Implementation approach: 
Using SDL as the game engine and C++ as the main programming language.

Approximate timeline:
Week 5: Finish and submit project proposal. Download required packages and set up development environment.
Week 6: Learn about SDL and build an outline of our project
Week 7: Basic framework of the project component
Week 8: Logic component of the game
Week 9: Embed User-Interface
Week 10: Debugging

Team Coordination: 
Weekly regular meeting to discuss individual progress and assign tasks. Code checking is required for each individual push. 


** Risk management plan (max 1 paragraph): 
This project has no significant risks
